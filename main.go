package main

import (
	"fmt"
	"os"
)

// Approach first
func SaveData1(path string, data []byte) error {

	fp, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0664)

	if err != nil {
		return err
	}

	defer fp.Close()

	_, err = fp.Write(data)

	return err
}

// Approach 2nd
func SaveData2(path string, data []byte) error {

	temp := fmt.Sprintf("%s.temp.%d", path, randomInt())

	fp, err := os.OpenFile(temp, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0664)

	if err != nil {
		return err
	}

	defer fp.Close()

	_, err1 := fp.Write(data)

	if err1 != nil {
		os.Remove(temp)
		return err
	}

	return os.Rename(temp, path)
}

func main() {
	fmt.Println("This is go main file!!")
}
